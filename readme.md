# Develop Webcomponents with stencil

🚨 **To use this template you have to install [docker](https://docker.com) on your device.** 🚨

## setup

1. clone this project
2. start docker compose using `docker compose up -d` in case you are using an older docker version the command will be `docker-compose up -d`.
3. start developing in `/src/components`.
4. take a look at your changes at [http://localhost:3333](http://localhost:3333)

## usage

\<write content here>

### NPM
`https://npm.appose.com/-/web/detail/@appose/<namespace>`
### cdn
module: `https://cdn.appose.com/competencymodel-webcomponent/<version>/<namespace>/<namespace>.esm.js`

## backlinks

[stencil readme](./readme_stencil.md)