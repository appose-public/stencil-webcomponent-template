import { Component, Prop, h, State } from '@stencil/core';
import { TranslationUtils } from '../../utils/translation';
import { format } from '../../utils/utils';

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.scss',
  shadow: true,
})
export class MyComponent {
  @Prop() language: string;
  @State() translations: any;
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  async componentWillRender() {
   this.translations = await TranslationUtils.fetchTranslations(this.language);
  }

  private getText(): string {
    return format(this.first, this.middle, this.last);
  }

  render() {
    return <div>Hello, World! I'm {this.getText()}, {this.translations.name_webcomponent_identifier}</div>;
  }
}
