export namespace TranslationUtils {
  export async function fetchTranslations(lang: string) {
    var locale = lang ? lang : 'en';
    const existingTranslations = JSON.parse(sessionStorage.getItem(`i18n.${locale}`));
    if (existingTranslations && Object.keys(existingTranslations).length > 0) {
      return existingTranslations;
    } else {
      try {
        const result = await fetch(`/i18n/${locale}.json`);
        if (result.ok) {
          const data = await result.json();
          sessionStorage.setItem(`i18n.${locale}`, JSON.stringify(data));
          return data;
        }
      } catch (exception) {
        console.error(`Error loading locale: ${locale}`, exception);
      }
    }
  }
}
