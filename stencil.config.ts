import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

export const config: Config = {
  namespace: 'stencil-sass',
  outputTargets: [
    {
      type: 'dist',
      copy: [{ src: 'i18n/*.json', dest: 'i18n', warn: true }],
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      copy: [{ src: 'i18n/*.json', dest: 'i18n', warn: true }],
      serviceWorker: null, // disable service workers
    },
  ],
  plugins: [
    sass(
      {
        injectGlobalPaths: [
          'src/globals/variables.scss',
          'src/globals/mixins.scss'
        ]
      }
    )
  ]
};
