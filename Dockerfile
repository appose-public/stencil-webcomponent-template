FROM node:lts-slim
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3333
ENTRYPOINT [ "npm", "start" ]